package com.example.marcok.raspetfeeder;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.marcok.raspetfeeder.MainActivity;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import static com.example.marcok.raspetfeeder.MainActivity.wifiModuleIp;

public class ControlActivity extends AppCompatActivity {

    Button setButton;
    EditText loadText;
    public static String CMD = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control);

        //initialize button
        setButton = (Button) findViewById(R.id.setButton);
        loadText = (EditText) findViewById(R.id.loadText);

        setButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ipInstance ipInstance = new ipInstance();
                ipInstance.setIpAddress(MainActivity.ipAddressString);
                //ipInstance.getIPandPort();

                Toast.makeText(ControlActivity.this, "Setting...", Toast.LENGTH_SHORT).show();

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Socket socket;
                            //if (socket.getInputStream() == null) {
                            InetAddress inetAddress = InetAddress.getByName(wifiModuleIp);
                            socket = new java.net.Socket("192.168.50.10", 24);
                            DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
                            StringBuilder sb = new StringBuilder();
                            sb.append(loadText.getText().toString());
                            dataOutputStream.write(sb.toString().getBytes());
                            dataOutputStream.close();


                            socket.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }
        });
    }

    Thread thread = new Thread(new Runnable() {
        @Override
        public void run() {
            try {
                Socket socket;
                //if (socket.getInputStream() == null) {
                InetAddress inetAddress = InetAddress.getByName(wifiModuleIp);
                socket = new java.net.Socket("192.168.50.10", 24);
                DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
                dataOutputStream.writeBytes(loadText.toString());
                dataOutputStream.close();
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    });

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.AutoFeed:
                startActivity(new Intent(ControlActivity.this,ControlActivity.class));
                return true;
            case R.id.Main_Menu:
                startActivity(new Intent(ControlActivity.this,MainDisplayActivity.class));
                return true;
            case R.id.IP_Settings:
                startActivity(new Intent(ControlActivity.this,MainActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
