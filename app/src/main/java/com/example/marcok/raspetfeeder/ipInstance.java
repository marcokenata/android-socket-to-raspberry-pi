package com.example.marcok.raspetfeeder;

import android.util.Log;

import static com.example.marcok.raspetfeeder.MainActivity.wifiModuleIp;
import static com.example.marcok.raspetfeeder.MainActivity.wifiModulePort;

public class ipInstance {

    private String ipAddress;

    public void getIPandPort()
    {
        //seeing ip and port which associates, and also logs ip and port
        String iPandPort = ipAddress;
        Log.d("MYTEST","IP String: "+ iPandPort);
        String temp[]= iPandPort.split(":");
        wifiModuleIp = temp[0];
        wifiModulePort = Integer.valueOf(temp[1]);
        Log.d("MY TEST","IP:" +wifiModuleIp);
        Log.d("MY TEST","PORT:"+wifiModulePort);
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }
}
