package com.example.marcok.raspetfeeder;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.Socket;

public class MainActivity extends AppCompatActivity {

    Button connect;
    EditText ipAddress;
    Socket myAppSocket = null;
    public static String wifiModuleIp = "";
    public static int wifiModulePort = 0;
    public static String CMD = "0";
    public static String ipAddressString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //initialize button
        connect = (Button) findViewById(R.id.Connect);
        ipAddress = (EditText) findViewById(R.id.IP_Address);

        ipAddressString = ipAddress.toString();

        //set onclick listener on button, which send socket message
        connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ipInstance ipInstance = new ipInstance();
                ipInstance.setIpAddress(ipAddressString);
                //ipInstance.getIPandPort();

                Toast.makeText(MainActivity.this, "Checking..", Toast.LENGTH_SHORT).show();

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Socket socket;
                            //if (socket.getInputStream() == null) {
                            InetAddress inetAddress = InetAddress.getByName(wifiModuleIp);
                            socket = new java.net.Socket("192.168.50.10", 24);
                            DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
                            StringBuilder sb = new StringBuilder();
                            sb.append("Test");
                            dataOutputStream.write(sb.toString().getBytes());
                            dataOutputStream.close();

                            socket.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();

                Toast.makeText(MainActivity.this, "Connected", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.AutoFeed:
                startActivity(new Intent(MainActivity.this,ControlActivity.class));
                return true;
            case R.id.Main_Menu:
                startActivity(new Intent(MainActivity.this,MainDisplayActivity.class));
                return true;
            case R.id.IP_Settings:
                startActivity(new Intent(MainActivity.this,MainActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



}
