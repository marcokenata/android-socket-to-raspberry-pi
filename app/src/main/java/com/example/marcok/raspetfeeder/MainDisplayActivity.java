package com.example.marcok.raspetfeeder;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.Socket;

import static com.example.marcok.raspetfeeder.MainActivity.wifiModuleIp;

public class MainDisplayActivity extends AppCompatActivity {

    TextView remainingText;
    Button feedButton;
    public static String CMD = "0";

    public static String getCMD() {
        return CMD;
    }

    public static void setCMD(String CMD) {
        MainDisplayActivity.CMD = CMD;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_display);

        remainingText = (TextView) findViewById(R.id.remainingText);
        feedButton = (Button) findViewById(R.id.feedButton);

        feedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ipInstance ipInstance = new ipInstance();
                ipInstance.setIpAddress(MainActivity.ipAddressString);
                //ipInstance.getIPandPort();

                Toast.makeText(MainDisplayActivity.this, "Feeding...", Toast.LENGTH_SHORT).show();

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Socket socket = null;
                        try {

                                //if (socket.getInputStream() == null) {
                                InetAddress inetAddress = InetAddress.getByName(wifiModuleIp);
                                socket = new Socket("192.168.50.10", 24);
                                DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
                                StringBuilder sb = new StringBuilder();
                                sb.append("Feed");
                                dataOutputStream.write(sb.toString().getBytes());
                                dataOutputStream.close();


                            } catch(IOException e){
                                e.printStackTrace();
                            } catch (Exception e){
                            try {
                                socket.close();
                            }catch (IOException e1){
                                e1.printStackTrace();
                            }
                        }
                        }
                }).start();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.AutoFeed:
                startActivity(new Intent(MainDisplayActivity.this,ControlActivity.class));
                return true;
            case R.id.Main_Menu:
                startActivity(new Intent(MainDisplayActivity.this,MainDisplayActivity.class));
                return true;
            case R.id.IP_Settings:
                startActivity(new Intent(MainDisplayActivity.this,MainActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
